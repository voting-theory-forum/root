NodeBB
======

NodeBB is a piece of free software to implement a discussion
forum.

I started documenting in plain text what I was doing to
install NodeBB, and I need to integrate that info into the
present rich-text document. But for now, I'll just say I'm
returning to NodeBB to configure it for mail.

## Mail ##

Log in through the web interface as the admin, and navigate
to https://staging.votingtheory.com/admin/settings/email .
Obviously, if you are working on production instead of
staging, sustitute accordingly.

| Parameter | Value |
|-----------|-------|
| Email Address | stage@votingtheory.org |
| From Name     | staging.votingtheory.com |
| Send X emails every Y milliseconds | leave at default |
| Use an external email server to send emails | no |

The last thing seems to have no effect when set to "no".
The rest of the form seems to assume you said "yes"
anyway.

Looking at available plugins. https://staging.votingtheory.com/admin/extend/plugins#download

Trying installing "nodebb-plugin-emailer-smtp".

Activated it (as advised).

Restarting NodeBB (as the user appropriate for staging or
production).
```
ssh stage@votingtheory.org
cd nodebb
./nodebb stop
./nodebb start
./nodebb log
```

