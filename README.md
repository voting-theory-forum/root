# Voting-theory Forum #

This repo is owned by an Interim Council that tasked itself
with the creation of an online discussion forum about voting
theory (= social-choice theory), and with setting up an
organization to run said discussion forum. For when the
organization will be real, the plan is to have a board
replace the interim council.
The discussion forum is to function
socially, substantially, as, in effect, successor to a
similar
forum that used to be run by the Center for Election
Science, but they are shutting their forum down.

The formation and composition of the Interim Council
is described on Reddit at
[this link](https://www.reddit.com/r/ElectionScience/comments/i9wt9z/founding_a_new_voting_theory_forum/).

This repo will document how we
set up a server, install the forum software and the pieces
on which it depends, and integrate and configure all those
parts so as to make the discussion forum work.

The file here "overall_seq.txt" describes the overall
sequence.

